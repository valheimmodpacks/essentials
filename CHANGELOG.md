# Essentials Changelog

## [Version 1.0.2] - 9-09-2023

### Added 
| Mod Name                             | Version   |
|--------------------------------------|-----------|
| AAA Crafting                         | 1.0.10    |
| FineWoodPieces                       | 1.1.1     |
| Valharvest                           | 3.0.4     |
| BoneAppetit                          | 3.3.0     |
| Fruit_Trees                          | 1.2.7     |
| SeedTotem                            | 4.3.2     |
| BruteWeapons                         | 1.0.3     |
| MagicalWeapons                       | 1.1.1     |
| Fantasy_Armoury                      | 0.1.7     |
| MagicHeim                            | 1.2.5     |
| Outsiders                            | 1.5.3     |
| TeleportEverything                   | 2.5.0     |
| PlanBuild                            | 0.14.2    |
| BetterUI_Reforged                    | 2.4.4     |

### Removed mods (Issues in game)
Issues while building structures
| Mod Name                             | Version   |
|--------------------------------------|-----------|
| BuildExpansion                       | 1.0.8     | 
| OdinArchitect_Custom_buildings       | 0.0.3     | 


## [Version 1.0.1] - 09-09-2023

### Updated mods
| Mod Name                             | Version   |
|--------------------------------------|-----------|
| Snap_Points_Made_Easy                | 1.3.2 -> 1.3.3|

### Broken packages removed
| Mod Name                             | Version   |
|--------------------------------------|-----------|
| ValheimCuisine                       | 2.0.3     |
| DvergrBuildings_VBBuild              | 1.0.2     |
| OdinArchitect_Custom_buildings       | 0.0.3     |

## [Version 1.0.0] - 07-09-2023

### Added 
| Mod Name                             | Version   |
|--------------------------------------|-----------|
| BepInExPack_Valheim                  | 5.4.2105  |
| AdventureBackpacks                   | 1.6.26    |
| Clock                                | 1.6.0     |
| EquipmentAndQuickSlots               | 2.1.7     |
| DualWield                            | 1.0.8     |
| OdinArchitect                        | 1.2.7     |
| MoreVanillaBuilds                    | 1.1.5     |
| InstantMonsterLootDrop               | 0.5.0     |
| Warfare                              | 1.5.0     |
| Clutter                              | 0.1.5     |
| Clutter_Sign_Component_Fix           | 1.0.0     |
| MoreGates                            | 1.0.11    |
| Armory                               | 1.0.6     |
| OdinShipPlus                         | 0.3.3     |
| AirAnimals                           | 0.1.4     |
| SeaAnimals                           | 0.1.9     |
| LandAnimals                          | 0.2.0     |
| FarmGrid                             | 0.5.0     |
| FuelEternal                          | 1.2.1     |
| PlantIt                              | 0.1.5     |
| BoatAdditions                        | 1.1.5     |
| AutoFeed                             | 0.8.0     |
| Fantasy_Creatures                    | 0.3.7     |
| HammerTime                           | 0.3.6     |
| Jotunn                               | 2.12.7    |
| Build_Camera_Custom_Hammers_Edition  | 1.2.0     |
| ValheimCuisine                       | 2.0.3     |  
| OdinsSteelworks                      | 0.1.16    |
| OdinsKingdom                         | 1.1.21    |
| Evasion                              | 1.0.2     |
| SmoothSave                           | 1.0.2     |
| WarfareFireAndIce                    | 1.0.1     |
| MonstrumAshlands                     | 1.0.0     |
| Monstrum                             | 1.2.1     |
| MonstrumDeepNorth                    | 1.0.0     |
| AzuCraftyBoxes                       | 1.2.3     |
| MissingPieces                         | 2.0.2     |
| Pageable_Build_Hud                   | 1.0.2     |
| DvergrBuildings_VBBuild              | 1.0.2     | 
| InstantBuildingRepair                 | 0.3.0     |
| Snap_Points_Made_Easy                | 1.3.3     | 
| BuildExpansion                        | 1.0.8     |
| OdinArchitect_Custom_buildings       | 0.0.3     |