# Valheim Essentials ModPack: A Comprehensive Modpack for Epic Adventures!

<p align="center">
  <img src="https://imageupload.io/ib/HwktjP7jDFSIl7Q_1694137935.png" 
     style="width: 70%"
     alt="EssentialsLogo" />
</p>

<p align="center">
  <img src="https://imageupload.io/ib/MyJyADp6qysO6i3_1694137970.png"
     style="width: 50%" 
     alt="Watermark" />
</p>


Immerse yourself in an entirely new Valheim experience with our meticulously curated modpack. Explore the boundless potential of this beloved Viking world with a collection of essential and immersive mods that will transform your adventure. From quality of life enhancements to thrilling new content, Valheim Enhanced has it all.

Unleash the full potential of Valheim with Valheim Enhanced. Whether you're a seasoned Viking or new to the realm, this modpack promises endless adventures and limitless creativity. Are you ready to embark on an unforgettable journey?

## **NOTE**

Please, always read the changelog table, there you can find removed broken packages or updates. 

### Author info

You can find me on Discord, DM me for questions, broken packages or help :)

| Contacts                             |                |
|--------------------------------------|----------------|
| Discord                              | SoulReaver#0387|


### Essentials Repository

Now you can create issues in the repo, I will fix any issues with this modpack, just let me know.

| Gitlab                             |
|--------------------------------------|
| [Essentials ModPack](https://gitlab.com/valheimmodpacks/essentials)    
                    
<p align="center">
  <img src="https://imageupload.io/ib/VK9iWQEgpWxSZEF_1694288883.png"
     alt="Watermark" />
</p>


## Mods included
| Mod Name                                      | Mod Description                                        | Link                                            |
| --------------------------------------------- | ------------------------------------------------------ | --------------------------------------------------- |
| BepInExPack_Valheim                           | BepInEx pack for Valheim. Preconfigured and includes unstripped Unity DLLs. | [Mod Link](https://valheim.thunderstore.io/package/denikson/BepInExPack_Valheim/) |
| AdventureBackpacks                            | A Valheim Mod to add a catalogue of Adventuring Backpacks to the Game. These packs will grow and become more useful as the game progresses. | [Mod Link](https://valheim.thunderstore.io/package/Vapok/AdventureBackpacks/) |
| Clock                                         | Adds a simple configurable clock widget to the screen showing the in-game time of day or a fuzzy representation like 'Late Afternoon' and optionally the current day. | [Mod Link](https://valheim.thunderstore.io/package/CrazyPony/Clock/) |
| EquipmentAndQuickSlots                       | Give equipped items their own dedicated inventory slots, plus three more hotkeyable quick slots. | [Mod Link](https://valheim.thunderstore.io/package/RandyKnapp/EquipmentAndQuickSlots/) |
| DualWield                                     | Twice the weapons, twice the fun.                     | [Mod Link](https://valheim.thunderstore.io/package/Smoothbrain/DualWield/) |
| OdinArchitect                                 | Odin Architect is a mod that will allow you to build more with more structures and on a larger scale. You will find here not only larger counterparts of basic buildings but also completely new ones. | [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/OdinArchitect/) |
| MoreVanillaBuilds                             | MoreVanillaBuilds (MVB) is a Valheim mod to make all vanilla prefabs buildable with the hammer (survival way). This mod is inspired by BetterCreative. | [Mod Link](https://valheim.thunderstore.io/package/BippityBoppityBoo/MoreVanillaBuilds/) |
| Warfare                                      | Fills the weapon, shields, and armor gaps in vanilla Valheim and extends up till Deep North and Ashlands with MANY unique extras! | [Mod Link](https://valheim.thunderstore.io/package/Therzie/Warfare/) |
| InstantMonsterLootDrop                       | Causes monsters to drop their loot and poof immediately. | [Mod Link](https://valheim.thunderstore.io/package/aedenthornNexusMods/InstantMonsterLootDrop/) |
| Clutter                                       | Adds a bunch of clutter and decor items.               | [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/Clutter/) |
| Clutter_Sign_Component_Fix                    | Meant to fix the issue with the Sign Component used on the Diaries used in the Clutter mod. This mod is meant to be used with the Clutter mod but might also fix other signs that are old. | [Mod Link](https://valheim.thunderstore.io/package/Azumatt/Clutter_Sign_Component_Fix/) |
| MoreGates                                     | This is a basic mod that adds more gates and gate-like objects! | [Mod Link](https://valheim.thunderstore.io/package/RagnarokHCRP/MoreGates/) |
| Armory                                        | Adds the armory forge that offers new variants of original armors that upgrade to the next biome. | [Mod Link](https://valheim.thunderstore.io/package/Therzie/Armory/) |
| OdinShipPlus                                  | Add to your game the most beautiful and functional boats, in all there are 13 boats in the cargo, speed, fishing, and war categories. (Donor version of OdinShip). | [Mod Link](https://valheim.thunderstore.io/package/Marlthon/OdinShipPlus/) |
| AirAnimals                                   | Adds 10 types of birds, 6 types of butterflies and 4 types of beetles to your game. | [Mod Link](https://valheim.thunderstore.io/package/Marlthon/AirAnimals/) |
| SeaAnimals                                   | Marine animals arrived in Valheim (Orca, Dolphin, Sharks, Crocodile, Turtles, Seal and Penguin) and with them new tasty meats. | [Mod Link](https://valheim.thunderstore.io/package/Marlthon/SeaAnimals/) |
| LandAnimals                                  | Land Animals Family arrived in Valheim (Brown Bear, Capybara, Fox, Deer, Hare, Moose, Wolf, Squirrel, Raccoon, and Boar). | [Mod Link](https://valheim.thunderstore.io/package/Marlthon/LandAnimals/) |
| FarmGrid                                     | Allows for all plants planted with the cultivator to automatically be snapped and placed into a grid in which they will have enough space to grow. | [Mod Link](https://valheim.thunderstore.io/package/SarcenNexusMods/FarmGrid/) |
| FuelEternal                                  | Fuel Eternal is a reimagining of TorchesEternal with updated support for stone ovens and hot tubs, alongside an adjustable config to enable/disable options of your choosing. | [Mod Link](https://valheim.thunderstore.io/package/Marf/FuelEternal/) |
| PlantIt                                      | Adds lots of buildable custom plants. Use the custom item called Shovel to place them. | [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/PlantIt/) |
| BoatAdditions                                | Adds 3 small and 3 new large boats with anchors and lamps and a boatyard that can help you on your journey. | [Mod Link](https://valheim.thunderstore.io/package/blacks7ar/BoatAdditions/) |
| AutoFeed                                     | Lets tameable animals eat from nearby containers.      | [Mod Link](https://valheim.thunderstore.io/package/ALY/AutoFeed/) |
| Fantasy_Creatures                            | Adds 15 different fantasy-themed creatures.            | [Mod Link](https://valheim.thunderstore.io/package/Horem/Fantasy_Creatures/) |
| HammerTime                                   | Moves all pieces from custom hammers to the vanilla hammer. | [Mod Link](https://valheim.thunderstore.io/package/MSchmoecker/HammerTime/) |
| Jotunn                                       | Valheim Library was created with the goal of making the lives of mod developers easier. It enables you to create mods for Valheim using an abstracted API so you can focus on the actual content creation. | [Mod Link](https://valheim.thunderstore.io/package/ValheimModding/Jotunn/) |
| Build_Camera_Custom_Hammers_Edition          | Adds a build camera to your vanilla and custom hammers. Taken over from the original mod by Azumatt. | [Mod Link](https://valheim.thunderstore.io/package/Azumatt/Build_Camera_Custom_Hammers_Edition/) |
| OdinsSteelworks                              | Steelcrafting and weaponsmithing! Crossbows, 2-handed swords, axes, and more! | [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/OdinsSteelworks/) |
| OdinsKingdom                                 | A custom piece mod by the OdinPlus team, this mod adds several castle construction pieces with a custom build tool. | [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/OdinsKingdom/) |
| Evasion                                      | Adds a skill that reduces the stamina usage of dodging. | [Mod Link](https://valheim.thunderstore.io/package/Smoothbrain/Evasion/) |
| SmoothSave                                   | Removes the freeze that happens if the world is being saved. | [Mod Link](https://valheim.thunderstore.io/package/Smoothbrain/SmoothSave/) |
| WarfareFireAndIce                            | Extends the Warfare end game weapon and crafting content for the Ashlands and Deep North biomes. | [Mod Link](https://valheim.thunderstore.io/package/Therzie/WarfareFireAndIce/) |
| MonstrumAshlands                             | Adds new monsters and new forsaken boss to fight and tame for the Ashlands biome. | [Mod Link](https://valheim.thunderstore.io/package/Therzie/MonstrumAshlands/) |
| Monstrum                                     | Adds new monsters and mini-bosses to fight and tame for original Valheim up till Mistlands. | [Mod Link](https://valheim.thunderstore.io/package/Therzie/Monstrum/) |
| MonstrumDeepNorth                            | Adds new monsters to fight, tame, and new locations to discover for the Deep North biome. | [Mod Link](https://valheim.thunderstore.io/package/Therzie/MonstrumDeepNorth/) |
| AzuCraftyBoxes                               | AzuCraftyBoxes is a Valheim mod that allows players to access and use resources from nearby containers when crafting and building, based on a configurable range and item restrictions. Restrictions are controlled by the yaml file Azumatt.AzuCraftyBoxes.yml. | [Mod Link](https://valheim.thunderstore.io/package/Azumatt/AzuCraftyBoxes/) |
| MissingPieces                                | A fix for what impedes your building fix.              | [Mod Link](https://valheim.thunderstore.io/package/BentoG/MissingPieces/) |
| InstantBuildingRepair                         | Lets you repair all building pieces in range using a hotkey or console command. | [Mod Link](https://valheim.thunderstore.io/package/Pineapple/InstantBuildingRepair/) |
| Snap_Points_Made_Easy                        | General quality of life building mod that allows you to cycle through various snap-points when targeting a piece in the game - without needing to point at them directly with the mouse! | [Mod Link](https://valheim.thunderstore.io/package/MathiasDecrock/Snap_Points_Made_Easy/) |
| AAA Crafting                        | Azu Anti-Arthritic Crafting. Gives you an input field to enter the amount of items you want to craft. Instead of having to click the craft button 100 times or a fancy plus button 5-10 times. | [Mod Link](https://valheim.thunderstore.io/package/Azumatt/AAA_Crafting/) |
| FineWoodPieces                        | Adds finewood variants of the wood build pieces, stone roof shingles, thatch walls, additional containers and clutters.| [Mod Link](https://valheim.thunderstore.io/package/blacks7ar/FineWoodPieces/) |
| Valharvest                        | New vegetables to plant and harvest, like potatoes, rice, tomatoes, pumpkins, etc, and new ingredients to prepare new foods! | [Mod Link](https://valheim.thunderstore.io/package/Frenvius/Valharvest/) |
| BoneAppetit                        | New food and cooking stations! | [Mod Link](https://valheim.thunderstore.io/package/RockerKitten/BoneAppetit/) |
| Fruit Trees                       | Adds Avocado, Coconut, Papaya, Peach, Lemon, Lime, Orange, Mango, Pear, Banana, Granny Smiths Apple, Pink Lady Apple, Cherry and Walnut Tree's. | [Mod Link](https://valheim.thunderstore.io/package/Horem/Fruit_Trees/) |
| Bamboozled                        | A Custom Building Mod with Growable Bamboo and Bamboo Pieces and more! | [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/Bamboozled/) |
| SeedTotem                        | Place a Seed totem in your fields to automatically plant seeds around it. Give the totem a whack to harvest all plants around it. | [Mod Link](https://valheim.thunderstore.io/package/MathiasDecrock/SeedTotem/) |
| BruteWeapons                        | A savage revamp of the two-handed weapon experience.| [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/BruteWeapons/) |
| MagicalWeapons                       | Magical Weapons to add to your game! | [Mod Link](https://valheim.thunderstore.io/package/MythikWolf/MagicalWeapons/) |
| Fantasy Armoury                        | Adds 33 fantasy themed Weapons and 9 Shields. | [Mod Link](https://valheim.thunderstore.io/package/Horem/Fantasy_Armoury/) |
| MagicHeim                        | A mod that will add a perfectly refined magic Classes System. | [Mod Link](https://valheim.thunderstore.io/package/KGvalheim/MagicHeim/) |
| Outsiders                        | Fill your world with creatures, summons and much more from realities outside. | [Mod Link](https://valheim.thunderstore.io/package/UnJohnny/Outsiders/) |
| TeleportEverything                        | This mod adds extra excitement and functionality to the portal system. Teleport wolves, boars, loxes and other allies. Transport ores, ingots, carts and even take enemies with you, or block players teleporting when enemies are nearby. | [Mod Link](https://valheim.thunderstore.io/package/OdinPlus/TeleportEverything/) |
| PlanBuild                        | PlanBuild enables you to plan, copy and share your building creations in Valheim with ease. Includes terrain tools and immersion items. | [Mod Link](https://valheim.thunderstore.io/package/MathiasDecrock/PlanBuild/) |
| BetterUI Reforged                        | Masa's BetterUI mod updated with bug fixes and new features | [Mod Link](https://valheim.thunderstore.io/package/thedefside/BetterUI_Reforged/) |
